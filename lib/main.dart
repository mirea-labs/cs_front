import 'package:cs_front/src/data/journal_record/client_journal_record.dart';
import 'package:cs_front/src/data/journal_record/repository_journal_record.dart';
import 'package:cs_front/src/data/student/client_student.dart';
import 'package:cs_front/src/data/student/repository_student.dart';
import 'package:cs_front/src/data/study_group/client_study_group.dart';
import 'package:cs_front/src/data/study_group/repository_study_group.dart';
import 'package:cs_front/src/domain/interactor_study_groups.dart';
import 'package:cs_front/src/domain/journal_interactor.dart';
import 'package:cs_front/src/models/domain/domain_host_holder.dart';
import 'package:cs_front/src/presentation/journal/route_journal.dart';
import 'package:cs_front/src/presentation/routes/study_groups/route_study_groups.dart';
import 'package:cs_front/src/presentation/routes/study_groups/router_study_groups.dart';
import 'package:cs_front/src/presentation/welcome/route_welcome.dart';
import 'package:cs_front/src/presentation/welcome/router_welcome.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

main() {
  setupLocator();
  runApp(JournalApp());
}

void setupLocator() {
  GetIt.I.registerLazySingleton(() => Dio()..interceptors.add(PrettyDioLogger()));

  GetIt.I.registerLazySingleton<StudentClient>(() => StudentClient(GetIt.I(), baseUrl: GetIt.I.get<HostHolder>().host));
  GetIt.I.registerLazySingleton<StudyGroupClient>(() => StudyGroupClient(GetIt.I(), baseUrl: GetIt.I.get<HostHolder>().host));
  GetIt.I.registerLazySingleton<JournalRecordClient>(() => JournalRecordClient(GetIt.I(), baseUrl: GetIt.I.get<HostHolder>().host));

  GetIt.I.registerLazySingleton<StudentRepository>(() => StudentRepository(GetIt.I()));
  GetIt.I.registerLazySingleton<StudyGroupRepository>(() => StudyGroupRepository(GetIt.I()));
  GetIt.I.registerLazySingleton<JournalRecordRepository>(() => JournalRecordRepository(GetIt.I()));

  GetIt.I.registerFactory<JournalInteractor>(() => JournalInteractor(GetIt.I(), GetIt.I(), GetIt.I()));
  GetIt.I.registerFactory<StudyGroupsInteractor>(() => StudyGroupsInteractor(GetIt.I()));

  GetIt.I.registerLazySingleton<WelcomeRouter>(() => WelcomeRouter(() => StudyGroupsRoute(GetIt.I.get(), GetIt.I.get())));
  GetIt.I.registerLazySingleton<StudyGroupsRouter>(() => StudyGroupsRouter((id) => JournalRoute.byStudyGroupId(GetIt.I.get(), id)));
}

class JournalApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.green, accentColor: Colors.deepOrange),
      home: WelcomeRoute(GetIt.I.get()),
    );
  }
}
