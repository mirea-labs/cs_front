import 'package:flutter/foundation.dart';

class JournalRecord {
  final int id;
  final String studentName;
  final String groupName;
  final String studyPlanName;
  final String mark;

  JournalRecord({
    @required this.id,
    @required this.studentName,
    @required this.groupName,
    @required this.studyPlanName,
    @required this.mark,
  });
}
