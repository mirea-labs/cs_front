class StudyGroup {
  final int id;
  final String name;

  StudyGroup(this.id, this.name);
}