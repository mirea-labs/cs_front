import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'model_study_group.g.dart';

@JsonSerializable(nullable: false)
class StudyGroupData {
    final int id;
    final String name;

  StudyGroupData({
    @required this.id,
    @required this.name,
  });

  factory StudyGroupData.fromJson(Map<String, dynamic> json) => _$StudyGroupDataFromJson(json);
  Map<String, dynamic> toJson() => _$StudyGroupDataToJson(this);
}
