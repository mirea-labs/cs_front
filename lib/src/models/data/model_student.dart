import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'model_student.g.dart';

@JsonSerializable(nullable: false)
class StudentData {
    final int id;
    final String name;
    final String surname;
    final String secondName;
    final int studyGroupId;

  StudentData({
    @required this.id,
    @required this.name,
    @required this.surname,
    @required this.secondName,
    @required this.studyGroupId,
  });

  factory StudentData.fromJson(Map<String, dynamic> json) => _$StudentDataFromJson(json);
  Map<String, dynamic> toJson() => _$StudentDataToJson(this);
}
