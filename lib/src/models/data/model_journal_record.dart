import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'model_journal_record.g.dart';

@JsonSerializable(nullable: false)
class JournalRecordData {
  final int id;
  final int count;
  final int studentId;
  final int studyPlanId;
  final int markId;
  final bool isInTime;

  JournalRecordData({
    @required this.id,
    @required this.count,
    @required this.studentId,
    @required this.studyPlanId,
    @required this.markId,
    @required this.isInTime,
  });

  factory JournalRecordData.fromJson(Map<String, dynamic> json) => _$JournalRecordDataFromJson(json);
  Map<String, dynamic> toJson() => _$JournalRecordDataToJson(this);
}
