import 'package:cs_front/src/models/domain/domain_host_holder.dart';
import 'package:cs_front/src/presentation/welcome/router_welcome.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class WelcomeRoute extends StatelessWidget {
  final TextEditingController _controller = TextEditingController();
  final WelcomeRouter _router;

  WelcomeRoute(this._router);

  @override
  Widget build(BuildContext context) {
    _controller.text = 'http://localhost:8080';
    return Scaffold(
      appBar: AppBar(
        title: Text('ПиЭ БГПС. Лабораторная работа'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: double.infinity,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'Добро пожаловать!',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                    Text(
                      'введите узел бэк-системы, чтобы продолжить',
                      style: Theme.of(context).textTheme.caption,
                    ),
                    TextField(
                      textAlign: TextAlign.center,
                      controller: _controller,
                    ),
                    RaisedButton(
                      child: Text('Войти'),
                      onPressed: () {
                        final String host = _controller.text;
                        GetIt.I.registerSingleton<HostHolder>(HostHolder(host));
                        _router.navigateToStudyGroups(context);
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
