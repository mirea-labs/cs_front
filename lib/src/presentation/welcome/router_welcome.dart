import 'package:cs_front/src/presentation/routes/study_groups/route_study_groups.dart';
import 'package:flutter/material.dart';

class WelcomeRouter {
  final StudyGroupsRoute Function() _studyGroupsRoute;

  WelcomeRouter(this._studyGroupsRoute);

  void navigateToStudyGroups(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (_) => _studyGroupsRoute()),
    );
  }
}
