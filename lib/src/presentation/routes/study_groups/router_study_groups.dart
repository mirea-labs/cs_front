import 'package:cs_front/src/presentation/journal/route_journal.dart';
import 'package:flutter/material.dart';

class StudyGroupsRouter {
  final JournalRoute Function(int studyGroupId) _journalRoute;

  StudyGroupsRouter(this._journalRoute);

  void navigateToStudyGroups(BuildContext context, int studyGroupId) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (_) => _journalRoute(studyGroupId)),
    );
  }
}
