import 'package:cs_front/src/domain/interactor_study_groups.dart';
import 'package:cs_front/src/models/domain/domain_study_group.dart';
import 'package:cs_front/src/presentation/routes/study_groups/router_study_groups.dart';
import 'package:flutter/material.dart';

class StudyGroupsRoute extends StatelessWidget {
  final StudyGroupsInteractor _interactor;
  final StudyGroupsRouter _router;

  const StudyGroupsRoute(this._interactor, this._router);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Учебные группы'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.filter_list),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.help),
              onPressed: () {},
            ),
          ],
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {},
          ),
        ),
        body: _futureBody(context));
  }

  FutureBuilder _futureBody(BuildContext context) {
    return FutureBuilder<List<StudyGroup>>(
      future: _interactor.getStudyGroups(),
      builder: (BuildContext context, AsyncSnapshot<List<StudyGroup>> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Container(
              child: Center(
                child: Text("Something wrond"),
              ),
            );
          }
          final records = snapshot.data;
          return _listJournalRecords(context: context, records: records);
        } else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }

  ListView _listJournalRecords({BuildContext context, List<StudyGroup> records}) {
    return ListView.builder(
      itemCount: records.length,
      itemBuilder: (BuildContext context, int index) {
        return _buildStudyGroup(context, records[index]);
      },
    );
  }

  Widget _buildStudyGroup(BuildContext context, StudyGroup studyGroup) {
    return GestureDetector(
      onTap: () => _router.navigateToStudyGroups(context, studyGroup.id),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: double.infinity,
            child: Row(
              children: <Widget>[
                Text(
                  studyGroup.name,
                  style: Theme.of(context).textTheme.headline6,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
