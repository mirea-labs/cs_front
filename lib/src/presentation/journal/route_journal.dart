import 'package:cs_front/src/domain/journal_interactor.dart';
import 'package:cs_front/src/models/domain/domain_journal_record.dart';
import 'package:flutter/material.dart';

class JournalRoute extends StatelessWidget {
  final int _studyGroupId;
  final int _studentId;
  final JournalInteractor _interactor;

  JournalRoute.byStudentId(this._interactor, this._studentId) : _studyGroupId = null;

  JournalRoute.byStudyGroupId(this._interactor, this._studyGroupId) : _studentId = null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('ПиЭ БГПС'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.filter_list),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.help),
              onPressed: () {},
            ),
          ],
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {},
          ),
        ),
        body: _futureBody(context));
  }

  FutureBuilder _futureBody(BuildContext context) {
    return FutureBuilder<List<JournalRecord>>(
      future: _studentId != null ? _interactor.getJournalRecordsByStudentId(_studentId) : _interactor.getJournalRecordsByStudyGroupId(_studyGroupId),
      builder: (BuildContext context, AsyncSnapshot<List<JournalRecord>> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Container(
              child: Center(
                child: Text("Something wrond"),
              ),
            );
          }
          final records = snapshot.data;
          return _listJournalRecords(context: context, records: records);
        } else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }

  ListView _listJournalRecords({BuildContext context, List<JournalRecord> records}) {
    return ListView.builder(
        itemCount: records.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildJournalRecord(context, records[index]);
        });
  }

  _buildJournalRecord(BuildContext context, JournalRecord value) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            value.studentName,
            textAlign: TextAlign.justify,
            style: Theme.of(context).textTheme.headline5,
          ),
          Text(
            value.studyPlanName,
            textAlign: TextAlign.justify,
            style: Theme.of(context).textTheme.headline6,
          ),
          Text(
            value.groupName,
            textAlign: TextAlign.justify,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          Text(
            'Оценка ${value.mark}',
            textAlign: TextAlign.justify,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ],
      ),
    );
  }
}
