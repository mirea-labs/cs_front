import 'package:cs_front/src/data/journal_record/client_journal_record.dart';
import 'package:cs_front/src/models/data/model_journal_record.dart';

class JournalRecordRepository {
  final JournalRecordClient _journalRecordClient;

  JournalRecordRepository(this._journalRecordClient);

  Future<List<JournalRecordData>> getJournalRecordsByStudentId(int studentId) {
    return _journalRecordClient.getJournalRecordsByStudentId(studentId);
  }

  Future<List<JournalRecordData>> getJournalRecordsByStudyGroupId(int studyGroupId) {
    return _journalRecordClient.getJournalRecordsByStudyGroupId(studyGroupId);
  }
  
}
