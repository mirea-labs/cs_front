import 'package:cs_front/src/models/data/model_journal_record.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'client_journal_record.g.dart';

@RestApi()
abstract class JournalRecordClient {

  factory JournalRecordClient(Dio dio, {String baseUrl}) = _JournalRecordClient;

  @GET("/journal/student/{id}")
  Future<List<JournalRecordData>> getJournalRecordsByStudentId(@Path('id') int id);

  @GET("/journal/group/{id}")
  Future<List<JournalRecordData>> getJournalRecordsByStudyGroupId(@Path('id') int id);
}