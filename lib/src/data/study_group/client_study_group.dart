import 'package:cs_front/src/models/data/model_study_group.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'client_study_group.g.dart';

@RestApi()
abstract class StudyGroupClient {
  factory StudyGroupClient(Dio dio, {String baseUrl}) = _StudyGroupClient;

  @GET("/group")
  Future<List<StudyGroupData>> getStudyGroups();

  @GET("/group/{id}")
  Future<StudyGroupData> getStudyGroup(@Path('id') int id);
}
