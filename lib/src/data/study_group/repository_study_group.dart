import 'package:cs_front/src/data/study_group/client_study_group.dart';
import 'package:cs_front/src/models/data/model_study_group.dart';

class StudyGroupRepository {
  final StudyGroupClient _studyGroupClient;

  StudyGroupRepository(this._studyGroupClient);

  Future<List<StudyGroupData>> getStudyGroups() {
    return _studyGroupClient.getStudyGroups();
  }

  Future<StudyGroupData> getStudyGroup(int id) {
    return _studyGroupClient.getStudyGroup(id);
  }
}
