import 'package:cs_front/src/data/student/client_student.dart';
import 'package:cs_front/src/models/data/model_student.dart';
import 'dart:async';

class StudentRepository {
  final StudentClient _studentClient;

  StudentRepository(this._studentClient);

  Future<List<StudentData>> getStudents() {
    return _studentClient.getStudnets();
  }

  Future<List<StudentData>> getStudentsByStudyGroup(int studyGroupId) {
    return _studentClient.getStudnetsByStudyGroupId(studyGroupId);
  }

  Future<StudentData> getStudent(int id) {
    return _studentClient.getStudnet(id);
  }
}
