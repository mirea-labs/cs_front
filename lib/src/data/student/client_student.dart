import 'package:cs_front/src/models/data/model_student.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'client_student.g.dart';

@RestApi()
abstract class StudentClient {
  factory StudentClient(Dio dio, {String baseUrl}) = _StudentClient;

  @GET("/student")
  Future<List<StudentData>> getStudnets();

  @GET("/student/group/{id}")
  Future<List<StudentData>> getStudnetsByStudyGroupId(@Path('id') int studyGroupId);

  @GET("/student/{id}")
  Future<StudentData> getStudnet(@Path('id') int studentId);
}
