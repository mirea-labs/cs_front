import 'package:cs_front/src/data/journal_record/repository_journal_record.dart';
import 'package:cs_front/src/data/student/repository_student.dart';
import 'package:cs_front/src/data/study_group/repository_study_group.dart';
import 'package:cs_front/src/models/data/model_journal_record.dart';
import 'package:cs_front/src/models/data/model_student.dart';
import 'package:cs_front/src/models/data/model_study_group.dart';
import 'package:cs_front/src/models/domain/domain_journal_record.dart';

class JournalInteractor {
  final StudentRepository _studentRepository;
  final StudyGroupRepository _studyGroupRepository;
  final JournalRecordRepository _journalRecordRepository;

  JournalInteractor(
    this._studentRepository,
    this._studyGroupRepository,
    this._journalRecordRepository,
  );

  Future<List<JournalRecord>> getJournalRecordsByStudentId(int studentId) async {
    List<JournalRecordData> journalRecordDatas = await _journalRecordRepository.getJournalRecordsByStudentId(studentId);
    StudentData studentData = await _studentRepository.getStudent(studentId);
    StudyGroupData studyGroupData = await _studyGroupRepository.getStudyGroup(studentData.studyGroupId);

    return journalRecordDatas
        .map((e) => JournalRecord(
            id: e.id,
            groupName: studyGroupData.name,
            mark: e.markId.toString(),
            studentName: "${studentData.surname} ${studentData.name} ${studentData.secondName}",
            studyPlanName: e.studyPlanId.toString()))
        .toList()
          ..sort((a, b) => a.groupName.compareTo(b.groupName));
  }

  Future<List<JournalRecord>> getJournalRecordsByStudyGroupId(int studyGroupId) async {
    List<JournalRecordData> journalRecordDatas = await _journalRecordRepository.getJournalRecordsByStudyGroupId(studyGroupId);
    List<StudentData> studentDatas = await _studentRepository.getStudentsByStudyGroup(studyGroupId);
    StudyGroupData studyGroupData = await _studyGroupRepository.getStudyGroup(studyGroupId);

    return journalRecordDatas
        .map((e) {
          StudentData studentData = studentDatas.firstWhere((element) => element.id == e.studentId);
          return JournalRecord(
            id: e.id,
            groupName: studyGroupData.name,
            mark: e.markId.toString(),
            studentName: "${studentData.surname} ${studentData.name} ${studentData.secondName}",
            studyPlanName: e.studyPlanId.toString());
        })
        .toList()
          ..sort((a, b) => a.groupName.compareTo(b.groupName));
  }
}
