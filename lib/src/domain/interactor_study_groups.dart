import 'package:cs_front/src/data/study_group/repository_study_group.dart';
import 'package:cs_front/src/models/data/model_study_group.dart';
import 'package:cs_front/src/models/domain/domain_study_group.dart';

class StudyGroupsInteractor {
  final StudyGroupRepository _studyGroupRepository;

  StudyGroupsInteractor(this._studyGroupRepository);

  Future<List<StudyGroup>> getStudyGroups() async {
    List<StudyGroupData> studyGroupDatas = await _studyGroupRepository.getStudyGroups();
    return studyGroupDatas.map((e) => StudyGroup(e.id, e.name)).toList()..sort((a, b) => a.name.compareTo(b.name));
  }
}
